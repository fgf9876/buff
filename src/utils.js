
let mod = function(val){
    return val % 13 == 0 ? 13 : val % 13;
}

let calCardScore = function(scoreA, scoreB, isUpdate){

    let x = mod(scoreA),y=mod(scoreB);
    let sum = 0;
    if(x == 11 || x == 12 || x == 10) sum += 0.5;
    if(x == 13) sum += 1;
    sum += (x+1);
    if(y == 11 || y == 12 || y == 10) sum += 0.5;
    if(y == 13) sum += 1;
    sum += (y+1);
    if(sum >= 20) return sum-20;
    if(sum < 10) return sum;
    else  return sum - 10;
    
}

export default calCardScore

//nsole.log(calCardScore(4,6))
