import Vue from 'vue'
import Vuex from 'vuex'
import Eos from 'eosjs'

Vue.use(Vuex)

export default new Vuex.Store({
       state: {
           scatter: '',
           network: '',
           account: '',
           banlance: '',
           timer: '',
           createData: '',
           fightData: '',
	   resultDetail: [],
           myresultDetail: [],
           readyDetail: [],
           myreadyDetail: []
       },
       mutations: {
           setscatter (state, scatter) {
           state.scatter = scatter
           },
           setnetwork (state, network) {
                state.network = network
           },
           createdata (state, createData) {
                state.createData = createData
                window.localStorage.setItem('createData', JSON.stringify(createData));
           },
           fightdata (state, fightData) {
                state.fightData = fightData
                window.localStorage.setItem('fightData', JSON.stringify(fightData));
           },
           setaccount (state, account) {
               state.account = account
           },
           updatedata (state, network) {
               state.timer = setInterval(async() => {
		    const eosOptions = {expireInSeconds: 60, verbose: false};
                    const eos = state.scatter.eos(network, Eos, eosOptions);
                    let readyData = await eos.getTableRows({
                        json: true,
                        code: "pythongolang",
                        scope: 'pythongolang',
                        table: 'bugyou',
                        limit: '200',
                       });
                     var arr1=[],arr2=[]
                     readyData.rows.forEach(function(value,index,array){
                          if(value.display == true){
                             arr1.push(value)
                          }
                          if(value.player1 == state.account_name || value.player2 == state.account_name){
                             arr2.push(value)
                          }
                     });
                    state.readyDetail = arr1; 
                    state.myreadyDetail = arr2; 
                    arr1=[]
                    arr2=[]
                    let resultData = await eos.getTableRows({
                        json: true,
                        code: "pythongolang",
                        scope: 'pythongolang',
                        table: 'result',
                        limit: '200',
                       });
                     var arr1=[],arr2=[]
                     resultData.rows.forEach(function(value,index,array){
                          if(value.display == true){
                             arr1.push(value)
                          }
                          if(value.player1 == state.account_name || value.player2 == state.account_name){
                             arr2.push(value)
                          }
                     });
                    state.resultDetail = arr1; 
                    state.myresultDetail = arr2; 

		}, 2000)
           }
       }
})
