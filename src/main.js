import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './router/routes'
import store from './store/'
import App from './App'

Vue.use(VueRouter)
const router = new VueRouter({
        routes
})

new Vue({
   router,
   store,
   render: h => h(App)
 }).$mount('#app')
