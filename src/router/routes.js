import App from '../App'
import gameComponent from '../page/gameComponent'
import meGameComponent from '../page/meGameComponent'
import ruleComponent from '../page/ruleComponent'
import tradeComponent from '../page/tradeComponent'
import fightComponent from '../page/fightComponent'
import createGameComponent from '../page/createGameComponent'

export default [
        { 
           path: "/",
           component:gameComponent
        },
        {
            path:"/game",
            component:gameComponent
        },
        {
            path:"/create",
            component:createGameComponent
        },
        {
            path:"/gate/:id",
            name:"gate",
            component:fightComponent
        },
        {
            path:"/rule",
            component:ruleComponent
        },
        {
            path:"/me",
            component:meGameComponent
        },
        {
            path:"/trade",
            component:tradeComponent
        }
]
